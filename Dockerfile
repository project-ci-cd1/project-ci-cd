# Utilisez une image de base officielle Node.js
FROM node:14

# Définissez le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copiez les fichiers de package.json et package-lock.json
COPY package*.json ./

# Installez les dépendances du projet
RUN npm install

# Copiez tous les fichiers du projet dans le conteneur
COPY . .

# Exposez le port sur lequel votre application sera accessible
EXPOSE 3000

# Commande pour démarrer l'application
CMD ["npm", "start"]
