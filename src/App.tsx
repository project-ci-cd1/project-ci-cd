import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ProfileCard from './components/ProfileCard';
import { Profile } from './types';
import './index.css'; 

const App: React.FC = () => {
  const [profiles, setProfiles] = useState<Profile[]>([]);
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const fetchProfiles = async () => {
      try {
        const response = await axios.get('https://randomuser.me/api/?results=20');
        const userProfiles = response.data.results.map((user: any) => ({
          name: `${user.name.first} ${user.name.last}`,
          email: user.email,
          picture: user.picture.large
        }));
        setProfiles(userProfiles);
      } catch (error) {
        console.error('Erreur lors de la récupération des profils :', error);
      }
    };

    fetchProfiles();
  }, []);

  const filteredProfiles = profiles.filter(profile =>
    profile.name.toLowerCase().includes(searchTerm.toLowerCase())
  );

  const handleDeleteProfile = (index: number) => {
    setProfiles(prevProfiles => prevProfiles.filter((_, i) => i !== index));
  };

  return (
    <div className="App">
      <h1>Trombinoscope</h1>
      <input
        className="SearchBar"
        type="text"
        placeholder="Rechercher..."
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <div className="ProfilesContainer">
        {filteredProfiles.map((profile, index) => (
          <ProfileCard
            key={index}
            profile={profile}
            onDelete={() => handleDeleteProfile(index)}
          />
        ))}
      </div>
    </div>
  );
};

export default App;