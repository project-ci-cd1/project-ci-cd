import React from 'react';
import { Profile } from '../types';

interface ProfileCardProps {
  profile: Profile;
  onDelete: () => void; 
}

const ProfileCard: React.FC<ProfileCardProps> = ({ profile, onDelete }) => {
    return (
      <div className="ProfileCard">
        <img src={profile.picture} alt={`Profil de ${profile.name}`} />
        <h2>{profile.name}</h2>
        <p>{profile.email}</p>
        <button onClick={onDelete} className="DeleteButton">Supprimer</button> {/* Ajoutez ce bouton */}
      </div>
    );
  };
  
  export default ProfileCard;