import React, { useState } from 'react';
import axios from 'axios';
import { Profile } from '../types';

interface ProfileFormProps {
  onNewProfile: (profile: Profile) => void;
}

const ProfileForm: React.FC<ProfileFormProps> = ({ onNewProfile }) => {
  const [loading, setLoading] = useState(false);

  const fetchRandomProfile = async () => {
    setLoading(true);
    try {
      const response = await axios.get('https://randomuser.me/api/');
      const userData = response.data.results[0];
      onNewProfile({
        name: `${userData.name.first} ${userData.name.last}`,
        email: userData.email,
        picture: userData.picture.large
      });
    } catch (error) {
      console.error('Erreur lors de la récupération du profil :', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <button onClick={fetchRandomProfile} disabled={loading}>
        {loading ? 'Chargement...' : 'Générer un Profil Aléatoire'}
      </button>
    </div>
  );
};

export default ProfileForm;
